/*
 * ObjCJSON.swift
 * CoreDataWithJSON
 *
 * Created by François Lamboley on 2020/5/8.
 * Copyright © 2020 Frizlab. All rights reserved.
 */

import Foundation

import GenericJSON



public class ObjCJSON : NSObject, NSCoding {
	
	public var json: JSON
	
	public init(value: JSON) {
		json = value
	}
	
	public required init?(coder: NSCoder) {
		guard let data = coder.decodeObject(forKey: "json") as? Data else {
			return nil
		}
		let jsonDecoder = JSONDecoder()
		guard let decodedJSON = try? jsonDecoder.decode(JSON.self, from: data) else {
			return nil
		}
		json = decodedJSON
	}
	
	public func encode(with coder: NSCoder) {
		let jsonEncoder = JSONEncoder()
		guard let data = try? jsonEncoder.encode(json) else {
			NSException(name: NSExceptionName(rawValue: "JSONEncoderFailure"), reason: "Cannot encode JSON", userInfo: nil).raise()
			return
		}
		coder.encode(data, forKey: "json")
	}
	
}
