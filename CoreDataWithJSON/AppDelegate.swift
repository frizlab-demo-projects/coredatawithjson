/*
 * AppDelegate.swift
 * CoreDataWithJSON
 *
 * Created by François Lamboley on 2020/5/8.
 * Copyright © 2020 Frizlab. All rights reserved.
 */

import Cocoa

import GenericJSON



@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {
		let context = persistentContainer.viewContext
		let myObject = MyObject(context: context)
		myObject.json = JSON(stringLiteral: "Hello world")
		try! context.save()
		
		let fetchRequest: NSFetchRequest<MyObject> = MyObject.fetchRequest()
		try! print(context.fetch(fetchRequest).map{ $0.json })
	}
	
	func applicationWillTerminate(_ aNotification: Notification) {
	}
	
	/* ***********************
	   MARK: - Core Data stack
	   *********************** */
	
	lazy var persistentContainer: NSPersistentContainer = {
		let container = NSPersistentContainer(name: "CoreDataWithJSON")
		container.loadPersistentStores(completionHandler: { (storeDescription, error) in
			if let error = error {
				fatalError("Unresolved error \(error)")
			}
		})
		return container
	}()
	
	/* *****************************************
	   MARK: - Core Data Saving and Undo support
	   ***************************************** */
	
	@IBAction func saveAction(_ sender: AnyObject?) {
		let context = persistentContainer.viewContext
		
		if !context.commitEditing() {
			NSLog("%@", "\(NSStringFromClass(type(of: self))) unable to commit editing before saving")
		}
		if context.hasChanges {
			do {
				try context.save()
			} catch {
				let nserror = error as NSError
				NSApplication.shared.presentError(nserror)
			}
		}
	}
	
	func windowWillReturnUndoManager(window: NSWindow) -> UndoManager? {
		return persistentContainer.viewContext.undoManager
	}
	
	func applicationShouldTerminate(_ sender: NSApplication) -> NSApplication.TerminateReply {
		let context = persistentContainer.viewContext
		
		if !context.commitEditing() {
			NSLog("%@", "\(NSStringFromClass(type(of: self))) unable to commit editing to terminate")
			return .terminateCancel
		}
		
		if !context.hasChanges {
			return .terminateNow
		}
		
		do {
			try context.save()
		} catch {
			let nserror = error as NSError
			
			let result = sender.presentError(nserror)
			if (result) {
				return .terminateCancel
			}
			
			let question = NSLocalizedString("Could not save changes while quitting. Quit anyway?", comment: "Quit without saves error question message")
			let info = NSLocalizedString("Quitting now will lose any changes you have made since the last successful save", comment: "Quit without saves error question info");
			let quitButton = NSLocalizedString("Quit anyway", comment: "Quit anyway button title")
			let cancelButton = NSLocalizedString("Cancel", comment: "Cancel button title")
			let alert = NSAlert()
			alert.messageText = question
			alert.informativeText = info
			alert.addButton(withTitle: quitButton)
			alert.addButton(withTitle: cancelButton)
			
			let answer = alert.runModal()
			if answer == .alertSecondButtonReturn {
				return .terminateCancel
			}
		}
		return .terminateNow
	}
	
}
