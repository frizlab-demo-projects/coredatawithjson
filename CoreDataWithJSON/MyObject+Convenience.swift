/*
 * MyObject+Convenience.swift
 * CoreDataWithJSON
 *
 * Created by François Lamboley on 2020/5/8.
 * Copyright © 2020 Frizlab. All rights reserved.
 */

import Foundation

import GenericJSON



extension MyObject {
	
	var json: JSON? {
		get {return objcJSON?.json}
		set {objcJSON = newValue.flatMap{ ObjCJSON(value: $0) }}
	}
	
}
